import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfsActivosPage } from './profs-activos.page';

const routes: Routes = [
  {
    path: '',
    component: ProfsActivosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfsActivosPageRoutingModule {}
