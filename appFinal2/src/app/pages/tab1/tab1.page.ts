import { Component } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { DataLocalService } from '../../services/data-local.service';
import { RouterLink, Router } from '@angular/router';
import { NavController, AlertController } from '@ionic/angular';

/* Conexión  con BD */
import { HttpClient } from '@angular/common/http';




@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  id: string;
  rfc: string;
  listado: any;
  x: any ;
  xid: any ;
  temp;
  xrfc: any ;
  temp2;
  constructor(
              private alertCtrl: AlertController,
              private barcodeScanner: BarcodeScanner,
              private dataLocal: DataLocalService,
              private router: Router,
              private navCtrl: NavController,
              private httpClient: HttpClient
              ) { }

///////////////////////////////////////////////////////////////////////



              /* ESCANEO DE PROFESORES Y GUARDADO DE DATOS*/
 async scan() {
    this.barcodeScanner.scan().then(barcodeData => {
      console.log('Barcode data', barcodeData);
      if ( !barcodeData.cancelled ) {

          /* Conexion con BD*/
        const url = 'http://www.zafiraconsulting.com.mx/app/profesorQr.php?hash=' + barcodeData;
        this.httpClient.get(url).subscribe(apiData => (this.x = apiData));

        if ( JSON.stringify(this.x) === '[{"nombre":"No se encontro el usuario"}]') {
          console.log (JSON.stringify(this.x));
          console.log ('Datos invalidos');
          this.presentAlert();
          } else {

            console.log ('Hola', JSON.stringify(this.x));
            console.log('Datos correctos');
            this.router.navigate(['/tabs']);
// 023d1753c02472224e71e2da0bb4be33s

      }
        this.dataLocal.guardarRegistro( barcodeData.text );
        /* Validamos que el profesor exista */
        this.dataLocal.guardarProfesor(barcodeData.text, 'QR');
        this.router.navigate(['/materiales']);
      }

     }).catch(err => {
         console.log('Error', err);
     });
  }

  /* Alerta error inicio de sesión */
  async presentAlert() {
    const alert =  await this.alertCtrl.create({
      header: 'Error',
      subHeader: 'Usuario no valido',
      // message: ' ',
      buttons: ['OK']
    });

    alert.present();
  }



   async registrarID( id: string ) {

    // console.log('Entre a ID');

    if ( !id ) {
      console.log('Error');
      throw new Error('No ha ingresado nada alv');
    }

    const url = 'http://zafiraconsulting.com.mx/app/profesorID.php?id=' + id ;
    this.httpClient.get(url).subscribe(apiData => (this.xid = apiData));
    this.temp = JSON.stringify(this.xid);
    // console.log (JSON.stringify(this.xid));
    // console.log ('Hola 123', this.temp );

    /* Validamos si existe el profesor */
    if ( this.temp === '[{"nombre":"No se encontro el usuario"}]' || !(this.temp) ) {
      // console.log (JSON.stringify(this.xid));
      // console.log ('Datos invalidos');
      this.presentAlert();
      this.temp = '';
      this.xid = '';
      } else {


        //  console.log ('Hola', JSON.stringify(this.xid));
        // console.log('Datos correctos');
        this.router.navigate(['/materiales']);

        this.dataLocal.guardarProfesor(id, 'id');
        this.temp = '';
        this.xid = '';
    /*
    Recibir un Input y enviar a la base
    this.dataLocal.guardarRegistro( barcodeData.text );
    */
  }
  }

  async registrarRFC( rfc: string ) {
    console.log('Entre a RFC');

    if ( !rfc ) {
      console.log('Error');
      throw new Error('No ha ingresado nada alv');
    }

    const url = 'http://zafiraconsulting.com.mx/app/profesorRFC.php?rfc=' + rfc ;
    this.httpClient.get(url).subscribe(apiData => (this.xrfc = apiData));
    this.temp2 = JSON.stringify(this.xrfc);
    // console.log (JSON.stringify(this.xid));
    // console.log ('Hola 123', this.temp );

    /* Validamos si existe el profesor */
    if ( this.temp2 === '[{"nombre":"No se encontro el usuario"}]' || !(this.temp2) ) {
      // console.log (JSON.stringify(this.xid));
      // console.log ('Datos invalidos');
      this.presentAlert();
      this.temp2 = '';
      this.xrfc = '';
      } else {

        //  console.log ('Hola', JSON.stringify(this.xid));
        // console.log('Datos correctos');
        this.dataLocal.guardarProfesor(rfc, 'rfc');
    /*
    Recibir un Input y enviar a la base
    this.dataLocal.guardarRegistro( barcodeData.text );
    */
  }

}


}
