import { Component, OnInit } from '@angular/core';
import { MatsService } from 'src/app/services/mats.service';
import { DataLocalService } from '../../services/data-local.service';
import { Lista } from 'src/app/models/lista.model';
import { Usuario } from '../../models/vars.model';

@Component({
  selector: 'app-resultado',
  templateUrl: './resultado.page.html',
  styleUrls: ['./resultado.page.scss'],
})
export class ResultadoPage implements OnInit {

  constructor( public matsService: MatsService,
               public dataLocal: DataLocalService
               ) { }




  ngOnInit() {
  }

  enviaraBD() {


  // Llave, Proyector, Control Bocina, Grabadora
    let llave = '0';      // 4,5,6
    let proyector = '0';  // 2
    let control = '0';    // 7,8,9
    let bocina = '0';     // 2
    let grabadora = '0';
    let idusuario = '0';
    let nombreusuario = '0';
    let textoprof = '0';
    let tipoprof = '0';

    /* GUARDAMOS NUESTROS MATERIALES EN UN ARREGLO  */
    const guarda: any[] = [0];


    const tam = this.dataLocal.guardamats.length;
    // console.log('El tamaño de este arreglo es de: ', tam );
    for ( let i = 0; i < tam; i++ ) {
      guarda[i] = this.dataLocal.guardamats[i];
      if (guarda[i].tipo === 'Llave') {
        llave = guarda[i].text;
        console.log('Guarde ', llave, 'En var llave');
      } else {
        if (guarda[i].tipo === 'Proyector') {
          proyector = guarda[i].text;
          console.log('Guarde ', proyector, 'En var proyector');
        } else {
          if (guarda[i].tipo === 'Control') {
            control = guarda[i].text;
            console.log('Guarde ', control, 'En var control');
          } else {
            if (guarda[i].tipo === 'Grabadora') {
              grabadora = guarda[i].text;
              console.log('Guarde ', grabadora, 'En var grabadora');
            } else {
              bocina =  guarda[i].text;
              console.log('Guarde ', bocina, 'En var bocina');
            }
          }
        }

      }


      // console.log(this.dataLocal.guardamats[i]);
      // console.log('Arreglo: ', i, ' ', guarda[i].text);
  }



   /* GUARDAMOS NUESTRO USUARIO */

    let usuario: any;
    usuario = this.dataLocal.guardauser[0];
    idusuario = usuario.id;
    console.log('Guarde ', idusuario, 'en var usuario');
    nombreusuario = usuario.nombre;
    console.log('Nombre el user: ', nombreusuario);

   /* GUARDAMOS NUESTRO PROFESOR */

    let profesor: any;
    profesor = this.dataLocal.guardaprof[0];
    textoprof = profesor.text;
    console.log('Guarde ', textoprof, 'en var profesor');
    tipoprof = profesor.tipo;
    console.log('Guarde ', tipoprof, 'en var tipoprofesor');

   /* ENVIAMOS VARIABLES A BD */

    console.log('Aqui se envia a BD las var');





}
}
