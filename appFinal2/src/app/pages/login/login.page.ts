import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { RouterLink, Router } from '@angular/router';

// import { IonLoading, IonButton, IonContent } from '@ionic/react';


/* imports para BD */

import { HttpClient } from '@angular/common/http';
// import { HTTP } from '@ionic-native/http/ngx';
// import { Http, Headers, RequestOptions } from '@angular/http';

// import 'rxjs/add/operator/map';
import { LoadingController, AlertController } from '@ionic/angular';
import { Registro } from '../../models/registro.model';
import { Logeo } from '../../models/logeo.model';
import { DataLocalService } from '../../services/data-local.service';


@Component({

  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})


export class LoginPage implements OnInit {

  // Variables
  private urlapi = 'https://www.zafiraconsulting.com.mx/app/loginQr.php';
  usuario: string;
  password: string;
  res = 0;
  loading: any;
  logeo = new Logeo('', '' );
  x: any;
  temp: string;


  // Fin variables



  //// CONTRUCTOR

constructor(private alertCtrl: AlertController,
            private httpClient: HttpClient,
            public loadingCtrl: LoadingController,
            private router: Router,
            public dataLocal: DataLocalService
    ) { }



ngOnInit() {
  }


onSubmitTemplate() {
    console.log('Form submit');
  }

  /* Activa formulario solo si se llenan los campos */
login(formulario: NgForm) {
    console.log(formulario.valid);
  }



 /* Alerta error inicio de sesión */
    async presentAlert() {
    const alert =  await this.alertCtrl.create({
      header: 'Error al iniciar sesión',
      subHeader: 'Revise su Usuario o Contraseña',
      // message: ' ',
      buttons: ['OK']
    });

    alert.present();
  }



    /* Logeo con Base de Datos  */
    signIn() {

           this.logeo  = {
           username: this.usuario,
           password: this.password
          };
           // console.log (this.logeo);

   /* Crear loader en lo que se valida la información */
           this.presentLoading('Un momento por favor');


          /* Enviar info a BD */

           const url = 'http://www.racni.com/app/login.php?id=' + this.usuario + '&password=' + this.password;
           this.httpClient.get(url).subscribe(apiData => (this.x = apiData));
           this.temp = JSON.stringify(this.x);
           setTimeout(() => {
            console.log(this.temp);
            // this.log();
          }, 30000);
          /* Validar info recibida  */
           if ( this.temp === '0' || !(this.temp) ) {
            console.log (JSON.stringify(this.x));
            console.log ('Datos invalidos');
            this.presentAlert();
            setTimeout(() => {
              this.loading.dismiss();
            }, 500 );
            return;
            }

           console.log('Subi exitosamente: ', this.usuario);
           console.log ('Hola', JSON.stringify(this.x));
           console.log('Datos correctos');

           this.dataLocal.guardarUsuario( this.usuario, JSON.stringify(this.x));
           this.router.navigate(['/tabs']);

        /* Se cierra el loading se muestra respuesta */
           setTimeout(() => {
          this.loading.dismiss();
        }, 500 );
      }


 async presentLoading( message: string ) {
   this.loading = await this.loadingCtrl.create({
    message
    // duration: 2000
  });
   return this.loading.present();
}

}











        /* loader = await this.loading.create({
           content: 'Cargando',
         });
         loader.present().then(() => { */

    /*this.http.post ('http:http:www.zafiraconsulting.com.mx/this.login.php', data, options)
    .map(res => res.json())
    .subscribe(async res => {
    console.log(res);
     Cerrar loader
    loader.dismiss(); */

          /* Validar respuesta */
/*
    if (res === 'Your; Login; success;') {
    const alert = await this.alertCtrl.create({
      header: 'Bienvenido',
      subHeader: (res),
      buttons: ['OK']
    });
    alert.present();
    } else {
      const alert = await this.alertCtrl.create({
        header: 'ERROR',
        subHeader: 'Su usuario o contraseña son invalidos',
        buttons: ['OK']
      });
      alert.present();
  }});
});
*/
