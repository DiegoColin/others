import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSegment } from '@ionic/angular';
import { MatsService } from '../../services/mats.service';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { DataLocalService } from '../../services/data-local.service';
import { RouterLink, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { ListaItem } from '../../models/lista-item.model';
import { Lista } from '../../models/lista.model';
import { Mats } from '../../models/vars.model';

@Component({
  selector: 'app-materiales',
  templateUrl: './materiales.page.html',
  styleUrls: ['./materiales.page.scss'],
})
export class MaterialesPage implements OnInit {
  data: any[] =  Array(5);
  nombreItem = '';
  d = new Date();

  @ViewChild(IonSegment, { static: true }) segment: IonSegment;

  constructor(public matsService: MatsService,
              private barcodeScanner: BarcodeScanner,
              public  dataLocal: DataLocalService,
              private router: Router,
              private navCtrl: NavController) { }


////////////////////////////////////////////////////////////////////////

agregarItem() {

  if (this.nombreItem.length === 0) {
    return;
  }
  const tipo = this.reconocetipo(this.nombreItem);
  this.dataLocal.guardarmats(this.nombreItem, tipo);
  // console.log(this.nombreItem);
/*
  const nuevoItem = new ListaItem( this.nombreItem);
  this.lista.items.push( nuevoItem );
*/
}

agregarItem0() {

  if (this.nombreItem.length === 0) {
    return;
  }
  this.dataLocal.guardarmats(this.nombreItem, 'Llave');
  // console.log(this.nombreItem);
/*
  const nuevoItem = new ListaItem( this.nombreItem);
  this.lista.items.push( nuevoItem );
*/
}

  scan() {
    this.barcodeScanner.scan().then(barcodeData => {
      console.log('Barcode data', barcodeData);
      if ( !barcodeData.cancelled ) {
        const tipos = this.reconocetipo(this.nombreItem);
        this.dataLocal.guardarmats(this.nombreItem, tipos);
        // this.dataLocal.guardarRegistro( barcodeData.text );
        this.nombreItem = barcodeData.text;
      }

     }).catch(err => {
         console.log('Error', err);
     });
  }



  delete( i: number ) {
   this.dataLocal.guardamats.splice( i, 1 );
  }


  reconocetipo(texto: string) {

    switch (texto.slice( 0, 2 )) {

      case 'pr': {
         return 'Proyector';
         break;
      }

      case 'bo': {
         return 'Control';
         break;
      }

      case 'gr': {
        return 'Grabadora';
        break;
     }

      default: {
         return 'Bocina';
         break;
      }

    }}



  ngOnInit() {
    this.segment.value = 'llaves';
  }

}
