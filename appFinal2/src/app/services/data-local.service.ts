import { Injectable } from '@angular/core';
import {Registro} from '../models/registro.model';
import { Usuario, Profesor, Mats } from '../models/vars.model';

@Injectable({
  providedIn: 'root'
})
export class DataLocalService {

  guardados: Registro [] = [];
  guardauser: Usuario [] = [];
  guardaprof: Profesor [] = [];
  guardamats: Mats [] = [];

  constructor() { }


  guardarRegistro(text: string) {
    const nuevoRegistro = new Registro (text);
    this.guardados.unshift(nuevoRegistro);
  }

/*
  guardarUsuario(text: string) {
    const nuevoUsuario = new Usuario (text);
    this.guardauser.unshift(nuevoUsuario);
  }
*/
  guardarUsuario(id: string, nombre: string) {
  const nuevoUsuario = new Usuario ( id , nombre );
  this.guardauser.unshift(nuevoUsuario);
}


  guardarProfesor(texte: string, tipo: string) {
    const nuevoProfesor = new Profesor ( texte, tipo );
    this.guardaprof.unshift(nuevoProfesor);

  }

  guardarmats(text: string, tipo: string) {
    const nuevoMaterial = new Mats ( text, tipo );
    this.guardamats.push(nuevoMaterial);
  }
}
