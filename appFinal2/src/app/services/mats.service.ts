import { Injectable } from '@angular/core';
import { Lista } from '../models/lista.model';
import { ListaItem } from '../models/lista-item.model';


@Injectable({
  providedIn: 'root'
})
export class MatsService {


  listas: Lista[] = [];

  constructor() {

    const lista1 = new Lista('Materiales Entrega');

    this.listas.push(lista1);

   }


}
