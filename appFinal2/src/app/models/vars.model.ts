


export class Usuario {

    public id: string;
    public nombre: string;

    constructor(id: string, nombre: string) {

        this.id = id;
        this.nombre = nombre;
}

}


export class Profesor {
    public text: string;
    public tipo: string;

    constructor(text: string, tipo: string) {

        this.text = text;
        this.tipo = tipo;
}
}

// Verificar con arreglos o crear clase por tabla de material
export class Mats {
    public text: string;
    public tipo: string;

    constructor(text: string, tipo: string) {

        this.text = text;
        this.tipo = tipo;
}
}
